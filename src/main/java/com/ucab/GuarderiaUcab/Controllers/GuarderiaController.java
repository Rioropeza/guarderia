package com.ucab.GuarderiaUcab.Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import com.ucab.GuarderiaUcab.Models.*;
import com.ucab.GuarderiaUcab.Views.*;

public class GuarderiaController implements ActionListener {
	public GuarderiaShowView show;
	public GuarderiaCreateView vistaCrear;
	public Guarderia guarderia;
	public Validaciones validar;
	
	public GuarderiaController (){
		this.guarderia = new Guarderia();
		this.validar = new Validaciones();
		this.initializerShowView();
	}
	private void initializerView( ) {

		this.vistaCrear = new GuarderiaCreateView();		
		this.vistaCrear.btnRegistrar.addActionListener(this);
		this.vistaCrear.btnRegresar.addActionListener(this);
		this.vistaCrear.FrInicio.setVisible(true);
	}
	
	private void initializerShowView( ) {
		
		show = new GuarderiaShowView();
		show.table.setModel(
		guarderia.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
		show.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                tableMouseClicked(e);
            }
        });
		this.show.btn_agregar.addActionListener(this);
	    this.show.FrInicio.setVisible(true);
	}
	
	public void validarGuarderia(GuarderiaCreateView vistaCrear){
		guarderia.setNombre(vistaCrear.txtNombre.getText());
		System.out.print(guarderia.getNombre());
		guarderia.setRif(vistaCrear.txtRif.getText());
		System.out.println(guarderia.getRif());
		guarderia.setTelefono(Integer.parseInt(vistaCrear.txtTelefono.getText()));
		guarderia.setTarifa(Integer.parseInt(vistaCrear.txtTarifa.getText()));
		
		
	
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if(this.show.btn_agregar == e.getSource()){
			initializerView( );
			
		}
		
		if (this.vistaCrear.btnRegistrar== e.getSource()){
			validarGuarderia(vistaCrear);
				if (guarderia.crearGuarderia()){
			  		System.out.println("hola");			  		
			  		vistaCrear.FrInicio.setVisible(false);
                    show.table.setModel(
                    guarderia.tableShow(show.col,show.btn_modificar,show.btn_eliminar));

				}
				else{
			  		System.out.println("adios");
				}
			
			
		}
	}
	
	private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
        
        int column = show.table.getColumnModel().getColumnIndexAtX(e.getX());
        int row = e.getY()/show.table.getRowHeight();
        Guarderia guarderia = new Guarderia();
        
        if(row < show.table.getRowCount() && row >= 0 && column < show.table.getColumnCount() && column >= 0){
            Object value = show.table.getValueAt(row, column);
            if(value instanceof JButton){
                ((JButton)value).doClick();
                JButton boton = (JButton) value;

                if(boton.getName().equals("m")){
                    System.out.println("Click en el boton modificar");
                    System.out.println(show.table.getValueAt(row, 0) );
                    //EVENTOS MODIFICAR
                }
                if(boton.getName().equals("e")){
                    JOptionPane.showConfirmDialog(null, "Desea eliminar esta Guarderia?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
                    System.out.println("Click en el boton eliminar");
                    guarderia.setRif(show.table.getValueAt(row, 0).toString());
                    guarderia.eliminarGuarderia();
                    show.table.setModel(
                    guarderia.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
                    //EVENTOS ELIMINAR
                }
              
            }

        }


    }
}
