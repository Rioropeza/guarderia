package com.ucab.GuarderiaUcab.Controllers;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;
import com.ucab.GuarderiaUcab.Views.*;


public class DashboardController  implements ActionListener {
	
	public DashboardView vista;
	
	
	public DashboardController(){
		//this.vista= new DashboardView();
		this.initializerView();
	}
	private void initializerView( ) {

		this.vista = new DashboardView();
		this.vista.btnGuarderia.addActionListener(this);
		this.vista.btnEmpleado.addActionListener(this);
		this.vista.btnRepresentante.addActionListener(this);
		this.vista.btnNino.addActionListener(this);
		this.vista.btnAutorizado.addActionListener(this);
		this.vista.btnJuego.addActionListener(this);
		this.vista.btnActividad.addActionListener(this);
		this.vista.btnEnfermedad.addActionListener(this);
		this.vista.btnMedicina.addActionListener(this);
		this.vista.btnAlergia.addActionListener(this);
		this.vista.btnSintoma.addActionListener(this);
		this.vista.FrInicio.setVisible(true);
	}
	
	
	public void actionPerformed(ActionEvent e) {
		if (this.vista.btnSintoma==e.getSource()){
			new GuarderiaController();
		} 
		if(this.vista.btnEmpleado==e.getSource()){
			new EmpleadoController();
		} 
		if(this.vista.btnRepresentante==e.getSource()){
			new  RepresentanteController();
		}
		if(this.vista.btnNino==e.getSource()){
			new NinoController();
		}
		if(this.vista.btnAutorizado ==e.getSource()){
			new AutorizadoController();
		}
		if(this.vista.btnJuego==e.getSource()){
			new JuegoController();
		}
	/*	if(this.vista.btnEnfermedad==e.getSource()){
			new EnfermedadController();
		}
		if(this.vista.btnMedicina==e.getSource()){
			new btnMedicinaController();
		}
		if(this.vista.btnAlergia==e.getSource()){
			new AlergiaController();
		}
		if (this.vista.btnSintoma==e.getSource()) {
			new SintomaController();
		}*/
	
	}

}
