package com.ucab.GuarderiaUcab.Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;


import com.ucab.GuarderiaUcab.Models.Representante;
import com.ucab.GuarderiaUcab.Models.Validaciones;

import com.ucab.GuarderiaUcab.Views.*;
import com.ucab.GuarderiaUcab.Views.RepresentanteCreateView;

public class RepresentanteController implements ActionListener {
		
		public RepresentanteShowView show;
		public RepresentanteCreateView vistaCrear;
		public Representante representante;
		public Validaciones validar;
		
		public RepresentanteController (){
			this.representante = new Representante();
			this.validar = new Validaciones();
			this.initializerShowView();
		}
		private void initializerView( ) {

			this.vistaCrear = new RepresentanteCreateView();		
			this.vistaCrear.btnRegistrar.addActionListener(this);
			this.vistaCrear.btnRegresar.addActionListener(this);
			this.vistaCrear.FrInicio.setVisible(true);
		}
		
		private void initializerShowView( ) {
			
			show = new RepresentanteShowView();
			show.table.setModel(
					representante.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
			show.table.addMouseListener(new MouseAdapter() {
	            public void mouseClicked(MouseEvent e) {
	                tableMouseClicked(e);
	            }
	        });
			this.show.btn_agregar.addActionListener(this);
		    this.show.FrInicio.setVisible(true);
		}
		
		public void validarRepresentante(RepresentanteCreateView vistaCrear){
			representante.setCi(Integer.parseInt(vistaCrear.txtCi.getText()));
			representante.setNombre(vistaCrear.txtNom.getText());
			representante.setApellido(vistaCrear.txtApe.getText());
			representante.setDireccion(vistaCrear.txtDireccion.getText());
			representante.setEdoCivil(vistaCrear.txtMn.getText());
			representante.setEmail(vistaCrear.txtEmail.getText());
			representante.setProfesion(vistaCrear.txtProfesion.getText());
			representante.setTelefonocasa(Integer.parseInt(vistaCrear.txtCasa.getText()));
			representante.setTelefonocell(Integer.parseInt(vistaCrear.txtCell.getText()));
			representante.setTelefonoofi(Integer.parseInt(vistaCrear.txtOficina.getText()));

			System.out.print("1"+representante.getNombre());
			System.out.print("2"+representante.getApellido() );
			System.out.print("3"+representante.getCi() );
			System.out.print("4"+representante.getEdoCivil() );
			
		}
		
		public void actionPerformed(ActionEvent e) {
			if(this.show.btn_agregar == e.getSource()){
				initializerView( );
				
			}
			
			if (this.vistaCrear.btnRegistrar== e.getSource()){
				validarRepresentante(vistaCrear);
					if (representante.creaRepresentante()){
				  		System.out.println("hola");			  		
				  		vistaCrear.FrInicio.setVisible(false);
	                    show.table.setModel(
	                    		representante.tableShow(show.col,show.btn_modificar,show.btn_eliminar));

					}
					else{
				  		System.out.println("adios");
					}
				
				
			}
		}
		
		private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
	        
	        int column = show.table.getColumnModel().getColumnIndexAtX(e.getX());
	        int row = e.getY()/show.table.getRowHeight();
	        Representante representante = new  Representante();
	        
	        if(row < show.table.getRowCount() && row >= 0 && column < show.table.getColumnCount() && column >= 0){
	            Object value = show.table.getValueAt(row, column);
	            if(value instanceof JButton){
	                ((JButton)value).doClick();
	                JButton boton = (JButton) value;

	                if(boton.getName().equals("m")){
	                    System.out.println("Click en el boton modificar");
	                    System.out.println(show.table.getValueAt(row, 0) );
	                    //EVENTOS MODIFICAR
	                }
	                if(boton.getName().equals("e")){
	                    JOptionPane.showConfirmDialog(null, "Desea eliminar este Representante?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
	                    System.out.println("Click en el boton eliminar");
	                    representante.setCi(Integer.parseInt(show.table.getValueAt(row, 0).toString()));
	                    representante.eliminarRepresentante();
	                    show.table.setModel(
	                    		representante.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
	                    //EVENTOS ELIMINAR
	                }
	              
	            }

	        }


		}
}



