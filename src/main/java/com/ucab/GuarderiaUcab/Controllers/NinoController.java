package com.ucab.GuarderiaUcab.Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import com.ucab.GuarderiaUcab.Models.Nino;
import com.ucab.GuarderiaUcab.Models.Validaciones;
import com.ucab.GuarderiaUcab.Views.NinoCreateView;
import com.ucab.GuarderiaUcab.Views.NinoShowView;

public class NinoController implements ActionListener {
	
	public NinoShowView show;
	public NinoCreateView vistaCrear;
	public Nino nino;
	public Validaciones validar;
	
	public NinoController (){
		this.nino = new Nino();
		this.validar = new Validaciones();
		this.initializerShowView();
	}
	private void initializerView( ) {

		this.vistaCrear = new NinoCreateView();		
		this.vistaCrear.btnRegistrar.addActionListener(this);
		this.vistaCrear.btnRegresar.addActionListener(this);
		this.vistaCrear.FrInicio.setVisible(true);
	}
	
	private void initializerShowView( ) {
		
		show = new NinoShowView();
		show.table.setModel(
				nino.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
		show.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                tableMouseClicked(e);
            }
        });
		this.show.btn_agregar.addActionListener(this);
	    this.show.FrInicio.setVisible(true);
	}
	
	public void validarNino(NinoCreateView vistaCrear){
		nino.setCodigo(vistaCrear.txtLetra.getText());
		nino.setNombre(vistaCrear.txtNombre.getText());
		nino.setApellido(vistaCrear.txtApellido.getText());
		nino.setFechaNac(vistaCrear.calendar.getDate());
		//nino.getFechaNac().setDate(new Date(vistaCrear.calendar.getDate().getTime())));
		nino.setSexo(vistaCrear.txtMf.getText());
	//	nino.setPediatra(vistaCrear.txtPediatra.getText());
	

		System.out.print(nino.getNombre());		
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if(this.show.btn_agregar == e.getSource()){
			initializerView( );
			
		}
		
		if (this.vistaCrear.btnRegistrar== e.getSource()){
			validarNino(vistaCrear);
				if (nino.creaNino()){
			  		System.out.println("hola");			  		
			  		vistaCrear.FrInicio.setVisible(false);
                    show.table.setModel(
                    		nino.tableShow(show.col,show.btn_modificar,show.btn_eliminar));

				}
				else{
			  		System.out.println("adios");
				}
			
			
		}
	}
	
	private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
        
        int column = show.table.getColumnModel().getColumnIndexAtX(e.getX());
        int row = e.getY()/show.table.getRowHeight();
        Nino nino = new  Nino();
        
        if(row < show.table.getRowCount() && row >= 0 && column < show.table.getColumnCount() && column >= 0){
            Object value = show.table.getValueAt(row, column);
            if(value instanceof JButton){
                ((JButton)value).doClick();
                JButton boton = (JButton) value;

                if(boton.getName().equals("m")){
                    System.out.println("Click en el boton modificar");
                    System.out.println(show.table.getValueAt(row, 0) );
                    //EVENTOS MODIFICAR
                }
                if(boton.getName().equals("e")){
                    JOptionPane.showConfirmDialog(null, "Desea eliminar este Representante?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
                    System.out.println("Click en el boton eliminar");
                    nino.setCodigo((show.table.getValueAt(row, 0).toString()));
                    nino.eliminarNino();
                    show.table.setModel(
                    		nino.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
                    //EVENTOS ELIMINAR
                }
              
            }

        }


	}
}
