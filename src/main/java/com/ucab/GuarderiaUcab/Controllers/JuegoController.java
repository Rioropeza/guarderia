package com.ucab.GuarderiaUcab.Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import com.ucab.GuarderiaUcab.Models.Juego;
import com.ucab.GuarderiaUcab.Models.Validaciones;
import com.ucab.GuarderiaUcab.Views.JuegoCreateView;
import com.ucab.GuarderiaUcab.Views.JuegoShowView;

public class JuegoController implements ActionListener {
	
	public JuegoShowView show;
	public JuegoCreateView vistaCrear;
	public Juego juego;
	public Validaciones validar;
	
	public JuegoController (){
		this.juego = new Juego();
		this.validar = new Validaciones();
		this.initializerShowView();
	}
	private void initializerView( ) {

		this.vistaCrear = new JuegoCreateView();		
		this.vistaCrear.btnRegistrar.addActionListener(this);
		this.vistaCrear.btnRegresar.addActionListener(this);
		this.vistaCrear.FrInicio.setVisible(true);
	}
	
	private void initializerShowView( ) {
		
		show = new JuegoShowView();
		show.table.setModel(
				juego.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
		show.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                tableMouseClicked(e);
            }
        });
		this.show.btn_agregar.addActionListener(this);
	    this.show.FrInicio.setVisible(true);
	}
	
	public void validarNino(JuegoCreateView vistaCrear){
		
		juego.setNombre(vistaCrear.txtNombre.getText());
		

		
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if(this.show.btn_agregar == e.getSource()){
			initializerView( );
			
		}
		
		if (this.vistaCrear.btnRegistrar== e.getSource()){
			validarNino(vistaCrear);
				if (juego.crearJuego()){
			  		System.out.println("hola");			  		
			  		vistaCrear.FrInicio.setVisible(false);
                    show.table.setModel(
                    		juego.tableShow(show.col,show.btn_modificar,show.btn_eliminar));

				}
				else{
			  		System.out.println("adios");
				}
			
			
		}
	}
	
	private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
        
        int column = show.table.getColumnModel().getColumnIndexAtX(e.getX());
        int row = e.getY()/show.table.getRowHeight();
        Juego juego = new  Juego();
        
        if(row < show.table.getRowCount() && row >= 0 && column < show.table.getColumnCount() && column >= 0){
            Object value = show.table.getValueAt(row, column);
            if(value instanceof JButton){
                ((JButton)value).doClick();
                JButton boton = (JButton) value;

                if(boton.getName().equals("m")){
                    System.out.println("Click en el boton modificar");
                    System.out.println(show.table.getValueAt(row, 0) );
                    //EVENTOS MODIFICAR
                }
                if(boton.getName().equals("e")){
                    JOptionPane.showConfirmDialog(null, "Desea eliminar este Representante?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
                    System.out.println("Click en el boton eliminar");
                    juego.setCodigo(Integer.parseInt(show.table.getValueAt(row, 0).toString()));
                    juego.eliminarJuego();
                    show.table.setModel(
                    		juego.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
                    //EVENTOS ELIMINAR
                }
              
            }

        }


	}
}
