package com.ucab.GuarderiaUcab.Controllers;



import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import com.ucab.GuarderiaUcab.Models.*;
import com.ucab.GuarderiaUcab.Views.*;

public class EmpleadoController implements ActionListener {
	public EmpleadoShowView show;
	public EmpleadoCreateView vistaCrear;
	public Empleado empleado;
	public Validaciones validar;
	
	public EmpleadoController (){
		this.empleado = new Empleado();
		this.validar = new Validaciones();
		this.initializerShowView();
	}
	private void initializerView( ) {

		this.vistaCrear = new EmpleadoCreateView();		
		this.vistaCrear.btnRegistrar.addActionListener(this);
		this.vistaCrear.btnRegresar.addActionListener(this);
		this.vistaCrear.FrInicio.setVisible(true);
	}
	
	private void initializerShowView( ) {
		
		show = new EmpleadoShowView();
		show.table.setModel(
				empleado.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
		show.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                tableMouseClicked(e);
            }
        });
		this.show.btn_agregar.addActionListener(this);
	    this.show.FrInicio.setVisible(true);
	}
	
	public void validarEmpleado(EmpleadoCreateView vistaCrear){
		empleado.setCi(Integer.parseInt(vistaCrear.txtCi.getText()));
		empleado.setNombre(vistaCrear.txtNombre.getText());
		empleado.setApellido(vistaCrear.txtApellido.getText());

		System.out.print(empleado.getNombre());
		
		
		empleado.setTelefono(Integer.parseInt(vistaCrear.txtTelefono.getText()));
		empleado.setEstudio(vistaCrear.txtEstudio.getText());
		empleado.setExperiencia(vistaCrear.txtExperiencia.getText());

		empleado.setSueldo(Double.parseDouble(vistaCrear.textField.getText()));
		
		
	
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if(this.show.btn_agregar == e.getSource()){
			initializerView( );
			
		}
		
		if (this.vistaCrear.btnRegistrar== e.getSource()){
			validarEmpleado(vistaCrear);
				if (empleado.creaEmpleado()){
			  		System.out.println("hola");			  		
			  		vistaCrear.FrInicio.setVisible(false);
                    show.table.setModel(
                    		empleado.tableShow(show.col,show.btn_modificar,show.btn_eliminar));

				}
				else{
			  		System.out.println("adios");
				}
			
			
		}
	}
	
	private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
        
        int column = show.table.getColumnModel().getColumnIndexAtX(e.getX());
        int row = e.getY()/show.table.getRowHeight();
        Empleado empleado = new  Empleado();
        
        if(row < show.table.getRowCount() && row >= 0 && column < show.table.getColumnCount() && column >= 0){
            Object value = show.table.getValueAt(row, column);
            if(value instanceof JButton){
                ((JButton)value).doClick();
                JButton boton = (JButton) value;

                if(boton.getName().equals("m")){
                    System.out.println("Click en el boton modificar");
                    System.out.println(show.table.getValueAt(row, 0) );
                    //EVENTOS MODIFICAR
                }
                if(boton.getName().equals("e")){
                    JOptionPane.showConfirmDialog(null, "Desea eliminar este empleado?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
                    System.out.println("Click en el boton eliminar");
                    empleado.setCi(Integer.parseInt(show.table.getValueAt(row, 0).toString()));
                    empleado.eliminarEmpleado();
                    show.table.setModel(
                    		empleado.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
                    //EVENTOS ELIMINAR
                }
              
            }

        }


    }
}
