package com.ucab.GuarderiaUcab.Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import com.ucab.GuarderiaUcab.Models.*;
import com.ucab.GuarderiaUcab.Views.*;

public class ActividadController implements ActionListener {
	public ActividadShowView show;
	public ActividadCreateView vistaCrear;
	public Actividad actividad;
	public Validaciones validar;
	
	public ActividadController (){
		this.actividad = new Actividad();
		this.validar = new Validaciones();
		this.initializerShowView();
	}
	private void initializerView( ) {

		this.vistaCrear = new ActividadCreateView();		
		this.vistaCrear.btnRegistrar.addActionListener(this);
		this.vistaCrear.btnRegresar.addActionListener(this);
		this.vistaCrear.FrInicio.setVisible(true);
	}
	
	private void initializerShowView( ) {
		
		show = new ActividadShowView();
		show.table.setModel(
		actividad.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
		show.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                tableMouseClicked(e);
            }
        });
		this.show.btn_agregar.addActionListener(this);
	    this.show.FrInicio.setVisible(true);
	}
	
	public void validarActividad(ActividadCreateView vistaCrear){
		actividad.setNombre(vistaCrear.txtNombre.getText());
		System.out.print(actividad.getNombre());
		
		actividad.setDescripcion(vistaCrear.textField.getText());
		
		if(vistaCrear.comboBox.getSelectedItem().equals(1)) {
			actividad.setEdadMinima(1);
		}else
			if(vistaCrear.comboBox.getSelectedItem().equals(2)) {
				actividad.setEdadMinima(2);
			}else
				if(vistaCrear.comboBox.getSelectedItem().equals(3)) {
					actividad.setEdadMinima(3);
				}
				else
					if(vistaCrear.comboBox.getSelectedItem().equals(4)) {
						actividad.setEdadMinima(4);
					}else
						if(vistaCrear.comboBox.getSelectedItem().equals(5)) {
							actividad.setEdadMinima(5);
						}else
							if(vistaCrear.comboBox.getSelectedItem().equals(6)) {
								actividad.setEdadMinima(6);
							}
					
			
		
	
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if(this.show.btn_agregar == e.getSource()){
			initializerView( );
			
		}
		
		if (this.vistaCrear.btnRegistrar== e.getSource()){
			validarActividad(vistaCrear);
				if (actividad.crearActividad()){
			  		System.out.println("hola");			  		
			  		vistaCrear.FrInicio.setVisible(false);
                    show.table.setModel(
                    actividad.tableShow(show.col,show.btn_modificar,show.btn_eliminar));

				}
				else{
			  		System.out.println("adios");
				}
			
			
		}
	}
	
	private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
        
        int column = show.table.getColumnModel().getColumnIndexAtX(e.getX());
        int row = e.getY()/show.table.getRowHeight();
        Actividad actividad = new Actividad();
        
        if(row < show.table.getRowCount() && row >= 0 && column < show.table.getColumnCount() && column >= 0){
            Object value = show.table.getValueAt(row, column);
            if(value instanceof JButton){
                ((JButton)value).doClick();
                JButton boton = (JButton) value;

                if(boton.getName().equals("m")){
                    System.out.println("Click en el boton modificar");
                    System.out.println(show.table.getValueAt(row, 0) );
                    //EVENTOS MODIFICAR
                }
                if(boton.getName().equals("e")){
                    JOptionPane.showConfirmDialog(null, "Desea eliminar esta actividad?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
                    System.out.println("Click en el boton eliminar");
                    actividad.setCodigo(Integer.parseInt(show.table.getValueAt(row, 0).toString()));
                    actividad.eliminarActividad();
                    show.table.setModel(
                    		actividad.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
                    //EVENTOS ELIMINAR
                }
              
            }

        }


    }
}
