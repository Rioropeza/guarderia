package com.ucab.GuarderiaUcab.Controllers;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import com.ucab.GuarderiaUcab.Models.*;
import com.ucab.GuarderiaUcab.Models.Validaciones;
import com.ucab.GuarderiaUcab.Views.*;

public class AutorizadoController implements ActionListener {
	public GuarderiaShowView show;
	public AutorizadoCreateView vistaCrear;
	public Autorizado autorizado;
	public Validaciones validar;
	
	public AutorizadoController (){
		this.autorizado = new Autorizado();
		this.validar = new Validaciones();
		this.initializerShowView();
	}
	private void initializerView( ) {

		this.vistaCrear = new AutorizadoCreateView();		
		this.vistaCrear.btnRegistrar.addActionListener(this);
		this.vistaCrear.btnRegresar.addActionListener(this);
		this.vistaCrear.FrInicio.setVisible(true);
	}
	
	private void initializerShowView( ) {
		
		show = new GuarderiaShowView();
		show.table.setModel(
				autorizado.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
		show.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                tableMouseClicked(e);
            }
        });
		this.show.btn_agregar.addActionListener(this);
	    this.show.FrInicio.setVisible(true);
	}
	
	public void validarAutorizado(AutorizadoCreateView vistaCrear){
		autorizado.setCi(Integer.parseInt(vistaCrear.txtCi.getText()));
		autorizado.setNombre(vistaCrear.txtNombre.getText());
		autorizado.setApellido(vistaCrear.txtApellido.getText());

		System.out.print(autorizado.getNombre());
		
		
		autorizado.setTelefonocasa(Integer.parseInt(vistaCrear.txtCasa.getText()));
		autorizado.setTelefonoofi(Integer.parseInt(vistaCrear.txtOficina.getText()));
		autorizado.setTelefonocell(Integer.parseInt(vistaCrear.txtCell.getText()));
		
		
	
		
	}
	
	public void actionPerformed(ActionEvent e) {
		if(this.show.btn_agregar == e.getSource()){
			initializerView( );
			
		}
		
		if (this.vistaCrear.btnRegistrar== e.getSource()){
			validarAutorizado(vistaCrear);
				if (autorizado.creaAutorizado()){
			  		System.out.println("hola");			  		
			  		vistaCrear.FrInicio.setVisible(false);
                    show.table.setModel(
                    		autorizado.tableShow(show.col,show.btn_modificar,show.btn_eliminar));

				}
				else{
			  		System.out.println("adios");
				}
			
			
		}
	}
	
	private void tableMouseClicked(MouseEvent e) {//GEN-FIRST:event_tablaMouseClicked
        
        int column = show.table.getColumnModel().getColumnIndexAtX(e.getX());
        int row = e.getY()/show.table.getRowHeight();
        Autorizado autorizado = new  Autorizado();
        
        if(row < show.table.getRowCount() && row >= 0 && column < show.table.getColumnCount() && column >= 0){
            Object value = show.table.getValueAt(row, column);
            if(value instanceof JButton){
                ((JButton)value).doClick();
                JButton boton = (JButton) value;

                if(boton.getName().equals("m")){
                    System.out.println("Click en el boton modificar");
                    System.out.println(show.table.getValueAt(row, 0) );
                    //EVENTOS MODIFICAR
                }
                if(boton.getName().equals("e")){
                    JOptionPane.showConfirmDialog(null, "Desea eliminar este autorizado?", "Confirmar", JOptionPane.OK_CANCEL_OPTION);
                    System.out.println("Click en el boton eliminar");
                    autorizado.setCi(Integer.parseInt(show.table.getValueAt(row, 0).toString()));
                    autorizado.eliminarAutorizado();
                    show.table.setModel(
                    		autorizado.tableShow(show.col,show.btn_modificar,show.btn_eliminar));
                    //EVENTOS ELIMINAR
                }
              
            }

        }


    }
}
