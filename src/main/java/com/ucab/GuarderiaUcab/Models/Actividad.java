package com.ucab.GuarderiaUcab.Models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.GuarderiaUcab.Dao.ActividadDao;

public class Actividad {
	private int codigo;
	private String nombre;
	private String descripcion;
	private int transporte;
	private int edadMinima;
	private int status;
	
	
	public int getCodigo() {
		return codigo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public int getTransporte() {
		return transporte;
	}
	public void setTransporte(int transporte) {
		this.transporte = transporte;
	}
	public int getEdadMinima() {
		return edadMinima;
	}
	public void setEdadMinima(int edadMinima) {
		this.edadMinima = edadMinima;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
public ActividadDao dao;
	
	public Actividad() {
		this.dao=new ActividadDao();
	}
	
	public boolean crearActividad() {
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
	}
	public void modificarActividad(){
		dao.update(this);

	}

	public ArrayList<Actividad> mostrarActividad(){
		ArrayList<Actividad> actividades = dao.readAll();
		return actividades;
	}


	public boolean eliminarActividad(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}

    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar){
    	ArrayList<Actividad>  actividades = mostrarActividad();
    	col = new Object[]{"Codigo","Nombre","Edad Minima","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	
    	for (Actividad actividad : actividades) {
    		tableModel.addRow(new Object[]{actividad.getCodigo(),actividad.getNombre(),actividad.getEdadMinima(),modificar,eliminar});
    	}
    	return tableModel;
    }
	
	
	
}
