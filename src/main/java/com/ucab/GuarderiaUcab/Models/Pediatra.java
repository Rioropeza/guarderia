package com.ucab.GuarderiaUcab.Models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import com.ucab.GuarderiaUcab.Dao.PediatraDao;

public class Pediatra {
	
	private String nombre;
	private int telefonocasa;
	private int telefonoofi;
	private int telefonocell;
	
	public int status;

	private PediatraDao dao;
	
	public Pediatra() {
		this.status=-1;
		this.dao= new PediatraDao();
		
	}
	
	public boolean creaPediatra(){
		dao.create(this);
		if (status>0)
			return true;
		else
			return false;

	}
	public void modificarPediatra(){
		dao.update(this);

	}

	public ArrayList<Pediatra> mostrarPediatra(){
		ArrayList<Pediatra> pediatras = dao.readAll();
		return pediatras;
	}
	
	

	public boolean eliminarAutorizado(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}
	
    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar){
    	ArrayList<Pediatra>  pediatras = mostrarPediatra();
    	col = new Object[]{"Nombre","telefono","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Pediatra pediatra : pediatras) {
    		tableModel.addRow(new Object[]{pediatra.getNombre(),pediatra.getTelefonocell(),modificar,eliminar});
    	}
    	return tableModel;
    }	



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getTelefonocasa() {
		return telefonocasa;
	}

	public void setTelefonocasa(int telefonocasa) {
		this.telefonocasa = telefonocasa;
	}

	public int getTelefonoofi() {
		return telefonoofi;
	}

	public void setTelefonoofi(int telefonoofi) {
		this.telefonoofi = telefonoofi;
	}

	public int getTelefonocell() {
		return telefonocell;
	}

	public void setTelefonocell(int telefonocell) {
		this.telefonocell = telefonocell;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
