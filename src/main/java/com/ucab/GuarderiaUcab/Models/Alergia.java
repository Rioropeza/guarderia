package com.ucab.GuarderiaUcab.Models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.GuarderiaUcab.Dao.AlergiaDao;

public class Alergia extends Salud{
	
	public AlergiaDao dao;
	
	public Alergia() {
		this.dao=new AlergiaDao();
	}
	
	public boolean crearAlergia() {
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
	}
	public void modificarAlergia(){
		dao.update(this);

	}

	public ArrayList<Alergia> mostrarAlergia(){
		ArrayList<Alergia> alergias = dao.readAll();
		return alergias;
	}


	public boolean eliminarAlergia(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}

    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar, JButton ver){
    	ArrayList<Alergia>  alergias = mostrarAlergia();
    	col = new Object[]{"Codigo","Nombre","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	
    	for (Alergia alergia : alergias) {
    		tableModel.addRow(new Object[]{alergia.getCod(),alergia.getNombre(),modificar,eliminar,ver});
    	}
    	return tableModel;
    }
}
