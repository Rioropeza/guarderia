package com.ucab.GuarderiaUcab.Models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import com.ucab.GuarderiaUcab.Dao.EmpleadoDao;

public class Empleado {
	
	private int ci;
	private String nombre;
	private String apellido;
	private String estudio;
	private String experiencia;
	private double sueldo;
	
	public double getSueldo() {
		return sueldo;
	}


	public void setSueldo(double sueldo) {
		this.sueldo = sueldo;
	}


	public String getEstudio() {
		return estudio;
	}


	public void setEstudio(String estudio) {
		this.estudio = estudio;
	}


	public String getExperiencia() {
		return experiencia;
	}


	public void setExperiencia(String experiencia) {
		this.experiencia = experiencia;
	}


	private int telefono;
	public int status ;

	private EmpleadoDao dao;


	public Empleado(){
		this.status = -1;
		this.dao= new EmpleadoDao();
	}

	
	public boolean creaEmpleado(){
		dao.create(this);
		if (status>0)
			return true;
		else
			return false;

	}
	public void modificarEmpleado(){
		dao.update(this);

	}

	public ArrayList<Empleado> mostrarEmpleado(){
		ArrayList<Empleado> empleados = dao.readAll();
		return empleados;
	}
	
	

	public boolean eliminarEmpleado(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}
	
    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar){
    	ArrayList<Empleado>  empleados = mostrarEmpleado();
    	col = new Object[]{"Cedula","Nombre","Apellido","sueldo","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Empleado empleado : empleados) {
    		tableModel.addRow(new Object[]{empleado.getCi(),empleado.getNombre(),empleado.getApellido(),modificar,eliminar});
    	}
    	return tableModel;
    }	


	public int getCi() {
		return ci;
	}


	public void setCi(int ci) {
		this.ci = ci;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellido() {
		return apellido;
	}


	public void setApellido(String apellido) {
		this.apellido = apellido;
	}


	public int getTelefono() {
		return telefono;
	}


	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}


	public int getStatus() {
		return status;
	}


	public void setStatus(int status) {
		this.status = status;
	}
	
	
}
