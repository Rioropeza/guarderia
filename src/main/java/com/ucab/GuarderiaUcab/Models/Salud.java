package com.ucab.GuarderiaUcab.Models;

public class Salud {

	protected int cod;
	protected String nombre;
	protected int status;
	
	public Salud() {
		this.status = -1;
	}

	public int getCod() {
		return cod;
	}

	public void setCod(int cod) {
		this.cod = cod;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
}
