package com.ucab.GuarderiaUcab.Models;

import java.util.ArrayList;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import com.ucab.GuarderiaUcab.Dao.NinoDao;

public class Nino {

	private String codigo;
	private String nombre;
	private String apellido;
	private Date fechaNac;
	private String sexo;
	private String pediatra;
	
	public int status ;
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	private NinoDao dao;
	
	public Nino() {
		this.status = -1;
		this.dao= new NinoDao();
	}
	
	public boolean creaNino(){
		dao.create(this);
		if (status>0)
			return true;
		else
			return false;

	}
	public void modificarNino(){
		dao.update(this);

	}

	public ArrayList<Nino> mostrarNino(){
		ArrayList<Nino> ninos = dao.readAll();
		return ninos;
	}
	
	
	public boolean eliminarNino(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}
	
    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar){
    	ArrayList<Nino>  ninos = mostrarNino();
    	col = new Object[]{"codigo","Nombre","Apellido","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Nino nino : ninos) {
    		tableModel.addRow(new Object[]{nino.getCodigo(),nino.getNombre(),nino.getApellido(),modificar,eliminar});
    	}
    	return tableModel;
    }	
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public Date getFechaNac() {
		return fechaNac;
	}
	public void setFechaNac(Date fechaNac) {
		this.fechaNac = fechaNac;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String string) {
		this.sexo = string;
	}

	public String getPediatra() {
		return pediatra;
	}

	public void setPediatra(String pediatra) {
		this.pediatra = pediatra;
	}
	
	

}
