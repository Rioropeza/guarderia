package com.ucab.GuarderiaUcab.Models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.GuarderiaUcab.Dao.SintomaDao;

public class Sintoma  extends Salud {
	
public SintomaDao dao;
	
	public Sintoma() {
		this.dao=new SintomaDao();
	}
	
	public boolean crearSintoma() {
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
	}
	public void modificarSintoma(){
		dao.update(this);

	}

	public ArrayList<Sintoma> mostrarSintoma(){
		ArrayList<Sintoma> sintomas = dao.readAll();
		return sintomas;
	}


	public boolean eliminarSintoma(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}

    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar, JButton ver){
    	ArrayList<Sintoma>  sintomas = mostrarSintoma();
    	col = new Object[]{"Codigo","Nombre","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	
    	for (Sintoma sintoma : sintomas) {
    		tableModel.addRow(new Object[]{sintoma.getCod(),sintoma.getNombre(),modificar,eliminar,ver});
    	}
    	return tableModel;
    }
}
