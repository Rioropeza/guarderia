package com.ucab.GuarderiaUcab.Models;


import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import com.ucab.GuarderiaUcab.Dao.*;

public class Autorizado {
	
	private int ci;
	private String nombre;
	private String apellido;
	private int telefonocasa;
	private int telefonoofi;
	private int telefonocell;

	public int status;


	private AutorizadoDao dao;


	public Autorizado(){
		this.status = -1;
		this.dao =new AutorizadoDao();
	}
	public boolean creaAutorizado(){
		dao.create(this);
		if (status>0)
			return true;
		else
			return false;

	}
	public void modificarAutorizado(){
		dao.update(this);

	}

	public ArrayList<Autorizado> mostrarAutorizado(){
		ArrayList<Autorizado> autorizados = dao.readAll();
		return autorizados;
	}
	
	

	public boolean eliminarAutorizado(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}
	
    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar){
    	ArrayList<Autorizado>  autorizados = mostrarAutorizado();
    	col = new Object[]{"Cedula","Nombre","Apellido","telefono","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Autorizado autorizado : autorizados) {
    		tableModel.addRow(new Object[]{autorizado.getCi(),autorizado.getNombre(),autorizado.getApellido(),autorizado.getTelefonocell(),modificar,eliminar});
    	}
    	return tableModel;
    }	




	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	public int getCi() {
		return ci;
	}
	public void setCi(int ci) {
		this.ci = ci;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public int getTelefonocasa() {
		return telefonocasa;
	}
	public void setTelefonocasa(int telefonocasa) {
		this.telefonocasa = telefonocasa;
	}
	public int getTelefonoofi() {
		return telefonoofi;
	}
	public void setTelefonoofi(int telefonoofi) {
		this.telefonoofi = telefonoofi;
	}
	public int getTelefonocell() {
		return telefonocell;
	}
	public void setTelefonocell(int telefonocell) {
		this.telefonocell = telefonocell;
	}
	
}
