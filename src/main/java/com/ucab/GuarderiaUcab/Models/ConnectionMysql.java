package com.ucab.GuarderiaUcab.Models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
//import com.mysql.cj.api.jdbc.Statement;

public class ConnectionMysql {

	protected Connection connection;
	protected ResultSet resultSet;
	protected PreparedStatement statement;

	public ConnectionMysql() {


	}
	
	public void connection() {
        try {
			System.out.println("instancia");
        	connection = DriverManager.getConnection ("jdbc:mysql://localhost:3306/guarderia2?autoReconnect=true&useSSL=false","root", "12345");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	
	
	
	public ResultSet openClonnectionExecute(String query) {
		try {
			System.out.println("abrio");
			connection();
			statement = connection.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			resultSet = statement.executeQuery();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return resultSet;
	}
	
	public int openClonnectionUpdate(String query) {
		System.out.println("abrio");
		connection();
		int rs = -1;
		try {
			statement = connection.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			rs = statement.executeUpdate();
		    //           getGeneratedKeys();
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return rs;
	}
	
	
	
	public void closeClonnection() {
		try {
			connection.close();
			System.out.println("cerro");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
