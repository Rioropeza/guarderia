package com.ucab.GuarderiaUcab.Models;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;
import com.ucab.GuarderiaUcab.Dao.GuarderiaDao;

public class Guarderia {

	protected String rif ;
	protected String nombre;
	protected String Direccion;
	protected int tarifa;
	protected int telefono ;
	protected int status;
	
	private GuarderiaDao dao;
	
	public Guarderia() {
		this.status = -1;
		this.dao = new GuarderiaDao();
	}
	
	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}

	public int getTarifa() {
		return tarifa;
	}

	public void setTarifa(int tarifa) {
		this.tarifa = tarifa;
	}
	
	
	
	public boolean crearGuarderia(){
		dao.create(this);
		if (status>0)
			return true;
		else
			return false;

	}

	public void modificarGuarderia(){
		dao.update(this);

	}

	public void buscarGuarderia(){


	}


	public ArrayList<Guarderia> mostrarGuarderia(){
		ArrayList<Guarderia> guarderias = dao.readAll();
		return guarderias;
	}


	public boolean eliminarGuarderia(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}

    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar){
    	ArrayList<Guarderia>  guarderias = mostrarGuarderia();
    	col = new Object[]{"Rif","Nombre","Telefono","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	
    	for (Guarderia guarderia : guarderias) {
    		tableModel.addRow(new Object[]{guarderia.getRif(),guarderia.getNombre(),guarderia.getTelefono(),modificar,eliminar});
    	}
    	return tableModel;
    }
	
    public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	public String getRif() {
		return rif;
	}
	public void setRif(String rif) {
		this.rif = rif;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getTelefono() {
		return telefono;
	}
	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}
}
