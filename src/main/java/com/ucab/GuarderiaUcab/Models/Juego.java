package com.ucab.GuarderiaUcab.Models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.GuarderiaUcab.Dao.JuegoDao;

public class Juego	 {
	private int codigo;
	private String nombre;
	private int status;

	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
	public JuegoDao dao;
	
	public Juego() {
		this.dao=new JuegoDao();
	}
	
	public boolean crearJuego() {
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
	}
	public void modificarJuego(){
		dao.update(this);

	}

	public ArrayList<Juego> mostrarJuego(){
		ArrayList<Juego> juegos = dao.readAll();
		return juegos;
	}


	public boolean eliminarJuego(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}

    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar  ){
    	ArrayList<Juego>  juegos = mostrarJuego();
    	col = new Object[]{"Codigo","Nombre","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	
    	for (Juego juego : juegos) {
    		tableModel.addRow(new Object[]{juego.getCodigo(),juego.getNombre(),modificar,eliminar});
    	}
    	return tableModel;
    }
}
