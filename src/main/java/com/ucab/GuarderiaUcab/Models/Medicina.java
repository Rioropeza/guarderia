package com.ucab.GuarderiaUcab.Models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.GuarderiaUcab.Dao.MedicinaDao;

public class Medicina extends Salud {
	public MedicinaDao dao;
	
	public Medicina() {
		this.dao=new MedicinaDao();
	}
	
	public boolean crearMedicina() {
		dao.create(this);
		if (status>0)
			return true;
		else 
			return false;
	}
	public void modificarMedicina(){
		dao.update(this);

	}

	public ArrayList<Medicina> mostrarMedicina(){
		ArrayList<Medicina> medicinas = dao.readAll();
		return medicinas;
	}


	public boolean eliminarMedicina(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}

    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar, JButton ver){
    	ArrayList<Medicina>  medicinas = mostrarMedicina();
    	col = new Object[]{"Codigo","Nombre","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	
    	for (Medicina medicina : medicinas) {
    		tableModel.addRow(new Object[]{medicina.getCod(),medicina.getNombre(),modificar,eliminar,ver});
    	}
    	return tableModel;
    }
}
