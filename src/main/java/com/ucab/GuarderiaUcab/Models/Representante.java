package com.ucab.GuarderiaUcab.Models;

import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.table.DefaultTableModel;

import com.ucab.GuarderiaUcab.Dao.RepresentanteDao;

public class Representante {

	private int ci;
	private String nombre;
	private String apellido;
	private String email;
	private int telefonocasa;
	private int telefonoofi;
	private int telefonocell;
	private String edoCivil;
	private String profesion;
	private String Direccion;
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public int status ;
	private RepresentanteDao dao;
	
	public Representante() {
		this.status = -1;
		this.dao= new RepresentanteDao();
	}
	
	public boolean creaRepresentante(){
		dao.create(this);
		if (status>0)
			return true;
		else
			return false;

	}
	public void modificarRepresentante(){
		dao.update(this);

	}

	public ArrayList<Representante> mostrarRepresentante(){
		ArrayList<Representante> representantes = dao.readAll();
		return representantes;
	}
	
	
	public boolean eliminarRepresentante(){
		dao.delete(this);
		if (status>0)
			return true;
		else
			return false;

	}
	
    public DefaultTableModel tableShow(Object[] col, JButton modificar, JButton eliminar){
    	ArrayList<Representante>  representantes = dao.readAll();
    	col = new Object[]{"Cedula","Nombre","Apellido","edo.civil","Acciones","",""};
    	DefaultTableModel tableModel = new DefaultTableModel(new Object [][] {},col);
    	for (Representante representante : representantes) {
    		tableModel.addRow(new Object[]{representante.getCi(),representante.getNombre(),representante.getApellido(),representante.getEdoCivil(),modificar,eliminar});
    	}
    	return tableModel;
    }	

	public int getCi() {
		return ci;
	}
	public void setCi(int ci) {
		this.ci = ci;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getTelefonocasa() {
		return telefonocasa;
	}
	public void setTelefonocasa(int telefonocasa) {
		this.telefonocasa = telefonocasa;
	}
	public int getTelefonoofi() {
		return telefonoofi;
	}
	public void setTelefonoofi(int telefonoofi) {
		this.telefonoofi = telefonoofi;
	}
	public int getTelefonocell() {
		return telefonocell;
	}
	public void setTelefonocell(int telefonocell) {
		this.telefonocell = telefonocell;
	}
	public String getEdoCivil() {
		return edoCivil;
	}
	public void setEdoCivil(String edoCivil) {
		this.edoCivil = edoCivil;
	}
	public String getProfesion() {
		return profesion;
	}
	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getDireccion() {
		return Direccion;
	}

	public void setDireccion(String direccion) {
		Direccion = direccion;
	}
}
