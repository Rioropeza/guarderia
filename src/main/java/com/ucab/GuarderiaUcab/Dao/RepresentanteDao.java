package com.ucab.GuarderiaUcab.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.GuarderiaUcab.Interfaces.Dao;
import com.ucab.GuarderiaUcab.Models.ConnectionMysql;
import com.ucab.GuarderiaUcab.Models.Representante;

public class RepresentanteDao extends ConnectionMysql implements Dao<Representante> {

	public void create(Representante representante) {
		 String	query = "INSERT into representante(pad_ci,pad_nombre,pad_apellido,pad_email,pad_telcasa,pad_telofi,pad_telcel,pad_profesion,pad_edocivil,fk_lu_cod)"+
		 "VALUES ('"+representante.getCi()+"','"+representante.getNombre()+"','"+representante.getApellido()+"','"+representante.getEmail()+"','"+representante.getTelefonocasa()+"','"+representante.getTelefonoofi()+"','"+representante.getTelefonocell()+"','"+representante.getProfesion()+"','"+representante.getEdoCivil()+"','"+1+"')";
		 representante.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
	}

	public void read(Representante representante) {
		ResultSet rs = openClonnectionExecute("Select* FROM representante WHERE pad_ci= '"+representante.getCi()+"' ");
		try{
			while (rs.next()) {
				representante.setCi(rs.getInt("pad_ci"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}

	public void update(Representante representante) {
		String	query = "INSERT into representante(pad_ci,pad_nombre,pad_apellido,pad_email,pad_telcasa,pad_telofi,pad_telcel,pad_profesion,pad_edocivil,fk_lu_cod)"+
		 "VALUES ('"+representante.getCi()+"','"+representante.getNombre()+"','"+representante.getApellido()+"','"+representante.getEmail()+"','"+representante.getTelefonocasa()+"','"+representante.getTelefonoofi()+"','"+representante.getTelefonocell()+"','"+representante.getProfesion()+"','"+representante.getEdoCivil()+"','"+1+")";
		 representante.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
	}

	public void delete(Representante representante) {
		representante.setStatus(openClonnectionUpdate("DELETE FROM representante WHERE pad_ci= '"+representante.getCi()+"' "));
		 closeClonnection();
	}

	public void report(Representante dao) {
		// TODO Auto-generated method stub
		
	}

	public ArrayList<Representante> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM representante");
		ArrayList<Representante> representantes = new ArrayList<Representante>();
		
		try{
			while (rs.next()) {
				Representante representante = new Representante();
				representante.setCi(rs.getInt("pad_ci"));
				representante.setNombre(rs.getString("pad_nombre"));
				representante.setApellido(rs.getString("pad_apellido"));
				representante.setEdoCivil(rs.getString("pad_edocivil"));
				representantes.add(representante);
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return representantes;
	}
}
