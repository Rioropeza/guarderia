package com.ucab.GuarderiaUcab.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.GuarderiaUcab.Interfaces.Dao;
import com.ucab.GuarderiaUcab.Models.ConnectionMysql;

import com.ucab.GuarderiaUcab.Models.Autorizado;

public class AutorizadoDao extends ConnectionMysql implements Dao<Autorizado> {

	public void create(Autorizado autorizado) {
		// TODO Auto-generated method stub
		String	query = "INSERT into autorizado(aut_ci,aut_nombre,aut_apellido,aut_telcasa,aut_telofi,aut_telcel)"+
		"VALUES ('"+autorizado.getCi()+"','"+autorizado.getNombre()+"','"+autorizado.getApellido()+"','"+autorizado.getTelefonocasa()+"','"+autorizado.getTelefonoofi()+"','"+autorizado.getTelefonocell()+"')";
		autorizado.setStatus(openClonnectionUpdate(query));
		closeClonnection();
	}

	public void read(Autorizado autorizado) {
		ResultSet rs = openClonnectionExecute("Select* FROM autorizado WHERE aut_ci= '"+autorizado.getCi()+"' ");
		try{
			while (rs.next()) {
				autorizado.setCi(rs.getInt("aut_ci"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}		
	}

	public void update(Autorizado autorizado) {
		String	query = "UPDATE into autorizado(aut_ci,aut_nombre,aut_apellido,aut_telcasa,aut_telofi,aut_telcel)"+		
		"VALUES ('"+autorizado.getCi()+"','"+autorizado.getNombre()+"','"+autorizado.getApellido()+"','"+autorizado.getTelefonocasa()+"','"+autorizado.getTelefonoofi()+"','"+autorizado.getTelefonocell()+"')";
		autorizado.setStatus(openClonnectionUpdate(query));
	}

	public void delete(Autorizado autorizado) {
		autorizado.setStatus(openClonnectionUpdate("DELETE FROM autorizado WHERE aut_ci= '"+autorizado.getCi()+"' "));
		 closeClonnection();
	}

	public void report(Autorizado dao) {
		// TODO Auto-generated method stub
		
	}
	public ArrayList<Autorizado> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM autorizado");
		ArrayList<Autorizado> autorizados = new ArrayList<Autorizado>();
		
		try{
			while (rs.next()) {
				Autorizado autorizado = new Autorizado();
				autorizado.setCi(rs.getInt("aut_ci"));
				autorizado.setNombre(rs.getString("aut_nombre"));
				autorizado.setApellido(rs.getString("aut_apellido"));
				autorizado.setTelefonocell(rs.getInt("aut_telcel"));
				autorizados.add(autorizado);
				System.out.println(rs.getString("aut_ci"));
				System.out.println(rs.getString("aut_nombre"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return autorizados;
	}

}
