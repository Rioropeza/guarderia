package com.ucab.GuarderiaUcab.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.GuarderiaUcab.Interfaces.Dao;
import com.ucab.GuarderiaUcab.Models.ConnectionMysql;
import com.ucab.GuarderiaUcab.Models.Empleado;


public class EmpleadoDao extends ConnectionMysql implements Dao<Empleado>{

	public void create(Empleado empleado) {
		 String	query = "INSERT into empleado(emp_ci,emp_nombre,emp_apellido,emp_telefono,emp_estudio,emp_experiencia,emp_sueldo)"+
		"VALUES ('"+empleado.getCi()+"','"+empleado.getNombre()+"','"+empleado.getApellido()+"','"+empleado.getTelefono()+"','"+empleado.getEstudio()+"','"+empleado.getExperiencia()+"','"+empleado.getSueldo()+"')";
		 empleado.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
		
	}

	public void read(Empleado empleado) {
		ResultSet rs = openClonnectionExecute("Select* FROM empleado WHERE emp_ci= '"+empleado.getCi()+"' ");
		try{
			while (rs.next()) {
				empleado.setCi(rs.getInt("emp_ci"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}
	

	public void update(Empleado empleado) {
		 String	query = "UPDATE into empleado(emp_ci,emp_nombre,emp_apellido,emp_telefono,emp_estudio,emp_experiencia,emp_sueldo)"+
		"VALUES('"+empleado.getCi()+"','"+empleado.getNombre()+"','"+empleado.getApellido()+"','"+empleado.getTelefono()+"','"+empleado.getEstudio()+"','"+empleado.getExperiencia()+"','"+empleado.getSueldo()+"')";
		 empleado.setStatus(openClonnectionUpdate(query));
		 closeClonnection();				
	}

	public void delete(Empleado empleado) {
		empleado.setStatus(openClonnectionUpdate("DELETE FROM empleado WHERE emp_ci= '"+empleado.getCi()+"' "));
		 closeClonnection();
	
	}

	public void report(Empleado dao) {
		// TODO Auto-generated method stub
		
	}

	public ArrayList<Empleado> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM empleado");
		ArrayList<Empleado> empleados = new ArrayList<Empleado>();
		
		try{
			while (rs.next()) {
				Empleado empleado = new Empleado();
				empleado.setCi(rs.getInt("emp_ci"));
				empleado.setNombre(rs.getString("emp_nombre"));
				empleado.setApellido(rs.getString("emp_apellido"));
			
				empleado.setSueldo(rs.getDouble("emp_sueldo"));
				empleados.add(empleado);

				System.out.println(rs.getString("emp_ci"));
				System.out.println(rs.getString("emp_nombre"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return empleados;
	}
}
