package com.ucab.GuarderiaUcab.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.GuarderiaUcab.Interfaces.Dao;
import com.ucab.GuarderiaUcab.Models.ConnectionMysql;
import com.ucab.GuarderiaUcab.Models.Guarderia;

public class GuarderiaDao extends ConnectionMysql implements Dao<Guarderia>{

	public void create(Guarderia guarderia) {
		// TODO Auto-generated method stub
		 String	query = "INSERT into guarderia(gua_rif,gua_nombre,gua_telefono,gua_tarifa,fk_lu_cod)"+
					"VALUES ('"+guarderia.getRif()+"','"+guarderia.getNombre()+"','"+guarderia.getTelefono()+"','"+guarderia.getTarifa()+"','"+1+"')";
		 guarderia.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
	}

	public void read(Guarderia guarderia) {
		// TODO Auto-generated method stub
		ResultSet rs = openClonnectionExecute("Select* FROM guarderia WHERE gua_rif= '"+guarderia.getRif()+"' ");
		try{
			while (rs.next()) {
				guarderia.setRif(rs.getString("gua_rif"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}

	public void update(Guarderia guarderia) {
		 String	query = "UPDATE into guarderia(gua_rif,gua_nombre,gua_telefono,gua_tarifa,fk_lu_cod)"+
		 "VALUES ('"+guarderia.getRif()+"','"+guarderia.getNombre()+"','"+guarderia.getTelefono()+"','"+guarderia.getTarifa()+"','"+1+"')";
		  guarderia.setStatus(openClonnectionUpdate(query));
		
	}

	public void delete(Guarderia guarderia) {
		guarderia.setStatus(openClonnectionUpdate("DELETE FROM guarderia WHERE gua_rif= '"+guarderia.getRif()+"' "));
		 closeClonnection();

	}	

	public void report(Guarderia dao) {
		// TODO Auto-generated method stub
		
	}
	
	public ArrayList<Guarderia> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM guarderia");
		ArrayList<Guarderia> guarderias = new ArrayList<Guarderia>();
		
		try{
			while (rs.next()) {
				Guarderia guarderia = new Guarderia();
				guarderia.setRif(rs.getString("gua_rif"));
			
				guarderia.setNombre(rs.getString("gua_nombre"));
			
				guarderia.setTelefono(rs.getInt("gua_telefono"));
				guarderias.add(guarderia);

				System.out.println(rs.getString("gua_rif"));
				System.out.println(rs.getString("gua_nombre"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return guarderias;
	}

}
