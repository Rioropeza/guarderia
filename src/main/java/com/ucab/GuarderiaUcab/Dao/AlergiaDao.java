package com.ucab.GuarderiaUcab.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.GuarderiaUcab.Interfaces.Dao;
import com.ucab.GuarderiaUcab.Models.Alergia;

import com.ucab.GuarderiaUcab.Models.ConnectionMysql;

public class AlergiaDao extends ConnectionMysql implements Dao<Alergia>{

	public void create(Alergia alergia) {
		String	query = "INSERT into alergia(ale_cod,ale_nombre)"+
		"VALUES ('"+alergia.getCod()+"','"+alergia.getNombre()+"')";
		alergia.setStatus(openClonnectionUpdate(query));
		closeClonnection();
		
	}

	public void read(Alergia alergia) {
		ResultSet rs = openClonnectionExecute("Select* FROM alergia WHERE ale_cod= '"+alergia.getCod()+"' ");
		try{
			while (rs.next()) {
				alergia.setCod(rs.getInt("ale_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}	
	}

	public void update(Alergia alergia) {
		String	query = "UPDATE into alergia(ale_cod,ale_nombre)"+
				"VALUES ('"+alergia.getCod()+"','"+alergia.getNombre()+"')";
				alergia.setStatus(openClonnectionUpdate(query));
				closeClonnection();		
	}

	public void delete(Alergia alergia) {
		ResultSet rs = openClonnectionExecute("DELETE FROM alergia WHERE ale_cod= '"+alergia.getCod()+"' ");
		try{
			while (rs.next()) {
				alergia.setCod(rs.getInt("ale_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
		
	}

	public void report(Alergia dao) {
		// TODO Auto-generated method stub
		
	}
	public ArrayList<Alergia> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM alergia");
		ArrayList<Alergia> alergias = new ArrayList<Alergia>();
		
		try{
			while (rs.next()) {
				Alergia alergia = new Alergia();
				alergia.setCod(rs.getInt("ale_cod"));
				alergias.add(alergia);
				alergia.setNombre(rs.getString("ale_nombre"));
				alergias.add(alergia);

				System.out.println(rs.getString("ale_cod"));
				System.out.println(rs.getString("ale_nombre"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return alergias;
	}
}
