package com.ucab.GuarderiaUcab.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.GuarderiaUcab.Interfaces.Dao;
import com.ucab.GuarderiaUcab.Models.ConnectionMysql;
import com.ucab.GuarderiaUcab.Models.Guarderia;
import com.ucab.GuarderiaUcab.Models.Pediatra;

public class PediatraDao extends ConnectionMysql implements Dao<Pediatra> {

	public void create(Pediatra pediatra) {
		String query = "INSERT into pediatra(ped_nombre,ped_telcasa,ped_telofi,ped_telcel)"+
		"VALUES ('"+pediatra.getNombre()+"','"+pediatra.getTelefonocasa()+"','"+pediatra.getTelefonoofi()+"','"+pediatra.getTelefonocell()+"')";
		pediatra.setStatus(openClonnectionUpdate(query));
		closeClonnection();
	}

	public void read(Pediatra pediatra) {
		ResultSet rs = openClonnectionExecute("Select* FROM pediatra WHERE ped_nombre= '"+pediatra.getNombre()+"' ");
		try{
			while (rs.next()) {
				pediatra.setNombre(rs.getString("ped_nombre"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}

	public void update(Pediatra pediatra) {
		String	query = "UPDATE into pediatra(ped_nombre,ped_telcasa,ped_telofi,ped_telcel)"+
		"VALUES ('"+pediatra.getNombre()+"','"+pediatra.getTelefonocasa()+"','"+pediatra.getTelefonoofi()+"','"+pediatra.getTelefonocell()+"')";
		pediatra.setStatus(openClonnectionUpdate(query));
						
	}

	public void delete(Pediatra pediatra) {
		pediatra.setStatus(openClonnectionUpdate("DELETE FROM pediatra WHERE ped_nombre= '"+pediatra.getNombre()+"' "));
		 closeClonnection();		
	}

	public void report(Pediatra dao) {
		// TODO Auto-generated method stub
		
	}
	public ArrayList<Pediatra> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM pediatra");
		ArrayList<Pediatra> pediatras = new ArrayList<Pediatra>();
		
		try{
			while (rs.next()) {
				Pediatra pediatra = new Pediatra();
				
				pediatra.setNombre(rs.getString("gua_nombre"));
				pediatras.add(pediatra);
				
				System.out.println(rs.getString("gua_nombre"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return pediatras;
	}

}
