package com.ucab.GuarderiaUcab.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.GuarderiaUcab.Interfaces.Dao;

import com.ucab.GuarderiaUcab.Models.ConnectionMysql;
import com.ucab.GuarderiaUcab.Models.Sintoma;

public class SintomaDao extends ConnectionMysql implements Dao<Sintoma> {
	
	public void create(Sintoma sintoma) {
		String	query = "INSERT into sintoma(sin_cod,sin_nombre)"+
		"VALUES ('"+sintoma.getCod()+"','"+sintoma.getNombre()+"')";
		sintoma.setStatus(openClonnectionUpdate(query));
		closeClonnection();
		
	}

	public void read(Sintoma sintoma) {
		ResultSet rs = openClonnectionExecute("Select* FROM sintoma WHERE sin_cod= '"+sintoma.getCod()+"' ");
		try{
			while (rs.next()) {
				sintoma.setCod(rs.getInt("ale_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}	
	}

	public void update(Sintoma sintoma) {
		String	query = "UPDATE into sintoma(sin_cod,sin_nombre)"+
				"VALUES ('"+sintoma.getCod()+"','"+sintoma.getNombre()+"')";
		sintoma.setStatus(openClonnectionUpdate(query));
				closeClonnection();		
	}

	public void delete(Sintoma sintoma) {
		ResultSet rs = openClonnectionExecute("DELETE FROM sintoma WHERE sin_cod= '"+sintoma.getCod()+"' ");
		try{
			while (rs.next()) {
				sintoma.setCod(rs.getInt("ale_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
		
	}

	public void report(Sintoma dao) {
		// TODO Auto-generated method stub
		
	}
	public ArrayList<Sintoma> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM sintoma");
		ArrayList<Sintoma> sintomas = new ArrayList<Sintoma>();
		
		try{
			while (rs.next()) {
				Sintoma sintoma = new Sintoma();
				sintoma.setCod(rs.getInt("sin_cod"));
				sintomas.add(sintoma);
				sintoma.setNombre(rs.getString("sin_nombre"));
				sintomas.add(sintoma);

				System.out.println(rs.getString("sin_cod"));
				System.out.println(rs.getString("sin_nombre"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return sintomas;
	}
}