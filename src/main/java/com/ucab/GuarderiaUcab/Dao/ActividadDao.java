package com.ucab.GuarderiaUcab.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.GuarderiaUcab.Interfaces.Dao;
import com.ucab.GuarderiaUcab.Models.ConnectionMysql;
import com.ucab.GuarderiaUcab.Models.Actividad;

public class ActividadDao extends ConnectionMysql implements Dao<Actividad>{
	
	public void create(Actividad actividad) {
		String	query = "INSERT into actividad(act_cod,act_nombre,act_descripcion,act_transporte,act_edadminima)"+
		"VALUES ('"+actividad.getCodigo()+"','"+actividad.getNombre()+"','"+actividad.getDescripcion()+"','"+0+"','"+actividad.getEdadMinima()+"')";
		actividad.setStatus(openClonnectionUpdate(query));
		closeClonnection();
		
	}

	public void read(Actividad actividad) {
		ResultSet rs = openClonnectionExecute("Select* FROM actividad WHERE act_cod= '"+actividad.getCodigo()+"' ");
		try{
			while (rs.next()) {
				actividad.setCodigo(rs.getInt("act_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}	
	}

	public void update(Actividad actividad) {
		String	query = "UPDATE into actividad(act_cod,act_nombre,act_descripcion,act_transporte,act_edadminima)"+
				"VALUES ('"+actividad.getCodigo()+"','"+actividad.getNombre()+"','"+actividad.getDescripcion()+"','"+0+"','"+actividad.getEdadMinima()+"')";
		actividad.setStatus(openClonnectionUpdate(query));
			closeClonnection();		
	}

	public void delete(Actividad actividad) {
		actividad.setStatus(openClonnectionUpdate("DELETE FROM actividad WHERE act_cod= '"+actividad.getCodigo()+"' "));
		 closeClonnection();
	}
	public void report(Actividad dao) {
		// TODO Auto-generated method stub
		
	}
	public ArrayList<Actividad> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM actividad");
		ArrayList<Actividad> actividades = new ArrayList<Actividad>();
		
		try{
			while (rs.next()) {
				Actividad actividad = new Actividad();
				actividad.setCodigo(rs.getInt("act_cod"));
				actividad.setNombre(rs.getString("act_nombre"));
				actividad.setEdadMinima(rs.getInt("act_edadminima"));
				
				actividades.add(actividad);

				System.out.println(rs.getString("act_cod"));
				System.out.println(rs.getString("act_nombre"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return actividades;
	}
}
