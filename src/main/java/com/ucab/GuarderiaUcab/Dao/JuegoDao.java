package com.ucab.GuarderiaUcab.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.GuarderiaUcab.Interfaces.Dao;
import com.ucab.GuarderiaUcab.Models.ConnectionMysql;
import com.ucab.GuarderiaUcab.Models.Juego;

public class JuegoDao extends ConnectionMysql implements Dao<Juego> {

	public void create(Juego juego) {
		String	query = "INSERT into juego(jue_cod,jue_nombre)"+
		"VALUES ('"+juego.getCodigo()+"','"+juego.getNombre()+"')";
		juego.setStatus(openClonnectionUpdate(query));
		closeClonnection();
		
	}

	public void read(Juego juego) {
		ResultSet rs = openClonnectionExecute("Select* FROM juego WHERE jue_cod= '"+juego.getCodigo()+"' ");
		try{
			while (rs.next()) {
				juego.setCodigo(rs.getInt("jue_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}	
	}

	public void update(Juego juego) {
		String	query = "UPDATE into alergia(jue_cod,jue_nombre)"+
			"VALUES ('"+juego.getCodigo()+"','"+juego.getNombre()+"')";
			juego.setStatus(openClonnectionUpdate(query));
			closeClonnection();		
	}

	public void delete(Juego juego) {
		juego.setStatus(openClonnectionUpdate("DELETE FROM juego WHERE jue_cod= '"+juego.getCodigo()+"' "));
		 closeClonnection();
	}

	public void report(Juego dao) {
		// TODO Auto-generated method stub
		
	}
	public ArrayList<Juego> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM juego");
		ArrayList<Juego> juegos = new ArrayList<Juego>();
		
		try{
			while (rs.next()) {
				Juego juego = new Juego();
				juego.setCodigo(rs.getInt("jue_cod"));
				
				juego.setNombre(rs.getString("jue_nombre"));
				juegos.add(juego);

				System.out.println(rs.getString("jue_cod"));
				System.out.println(rs.getString("jue_nombre"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return juegos;
	}
}
