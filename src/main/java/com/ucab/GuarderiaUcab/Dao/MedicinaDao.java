package com.ucab.GuarderiaUcab.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.GuarderiaUcab.Interfaces.Dao;

import com.ucab.GuarderiaUcab.Models.ConnectionMysql;
import com.ucab.GuarderiaUcab.Models.Medicina;

public class MedicinaDao extends ConnectionMysql implements Dao<Medicina> {

	public void create(Medicina medicina) {
		String	query = "INSERT into medicina (med_cod,med_nombre)"+
		"VALUES ('"+medicina.getCod()+"','"+medicina.getNombre()+"')";
		medicina.setStatus(openClonnectionUpdate(query));
		closeClonnection();
			
	}

	public void read(Medicina medicina) {
		ResultSet rs = openClonnectionExecute("Select* FROM medicina WHERE ale_cod= '"+medicina.getCod()+"' ");
		try{
			while (rs.next()) {
				medicina.setCod(rs.getInt("med_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}	
	}

	public void update(Medicina medicina) {
		String	query = "UPDATE into medicina(med_cod,med_nombre)"+
		"VALUES ('"+medicina.getCod()+"','"+medicina.getNombre()+"')";
		medicina.setStatus(openClonnectionUpdate(query));
		closeClonnection();		
	}

	public void delete(Medicina medicina) {
		ResultSet rs = openClonnectionExecute("DELETE FROM medicina WHERE ale_cod= '"+medicina.getCod()+"' ");
		try{
			while (rs.next()) {
				medicina.setCod(rs.getInt("med_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
		
	}

		public void report(Medicina dao) {
			// TODO Auto-generated method stub
			
		}
	public ArrayList<Medicina> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM medicina");
		ArrayList<Medicina> medicinas = new ArrayList<Medicina>();
		
		try{
			while (rs.next()) {
				Medicina medicina = new Medicina();
				medicina.setCod(rs.getInt("med_cod"));
				medicinas.add(medicina);
				medicina.setNombre(rs.getString("med_nombre"));
				medicinas.add(medicina);

				System.out.println(rs.getString("med_cod"));
				System.out.println(rs.getString("med_nombre"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return medicinas;
	}
}
