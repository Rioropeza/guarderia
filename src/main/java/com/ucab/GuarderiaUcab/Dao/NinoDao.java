package com.ucab.GuarderiaUcab.Dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ucab.GuarderiaUcab.Interfaces.Dao;
import com.ucab.GuarderiaUcab.Models.ConnectionMysql;
import com.ucab.GuarderiaUcab.Models.Nino;

public class NinoDao extends ConnectionMysql implements Dao<Nino> {

	public void create(Nino nino) {
		// TODO Auto-generated method stub
		 String	query = "INSERT into nino(nin_cod,nin_nombre,nin_apellido,nin_fnac,nin_sexo,fk_ped_nombre)"+
				"VALUES ('"+nino.getCodigo()+"','"+nino.getNombre()+"','"+nino.getApellido()+"','2012/12/12','"+nino.getSexo()+"','"+1+"')";
		 nino.setStatus(openClonnectionUpdate(query));
		 closeClonnection();
	}

	public void read(Nino nino) {
		// TODO Auto-generated method stub
		ResultSet rs = openClonnectionExecute("Select* FROM nino WHERE nin_cod= '"+nino.getCodigo()+"' ");
		try{
			while (rs.next()) {
				nino.setCodigo(rs.getString("nin_cod"));
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
	}

	public void update(Nino nino) {
		// TODO Auto-generated method stub
		 String	query = "UPDATE into nino(nin_cod,nin_nombre,nin_apellido,nin_fnac,nin_sexo,fk_ped_nombre)"+
		 "VALUES('"+nino.getCodigo()+"','"+nino.getNombre()+"','"+nino.getApellido()+"','"+nino.getFechaNac()+"','"+nino.getSexo()+"','"+nino.getPediatra()+"')";
	}

	public void delete(Nino nino) {
		nino.setStatus(openClonnectionUpdate("DELETE FROM nino WHERE nin_cod= '"+nino.getCodigo()+"' "));
		 closeClonnection();
	}

	public void report(Nino dao) {
		// TODO Auto-generated method stub
		
	}
	public ArrayList<Nino> readAll() {
		ResultSet rs = openClonnectionExecute("Select* FROM nino");
		ArrayList<Nino> ninos = new ArrayList<Nino>();
		
		try{
			while (rs.next()) {
				Nino nino = new Nino();
				nino.setCodigo(rs.getString("nin_cod"));
				nino.setNombre(rs.getString("nin_nombre"));
				nino.setApellido(rs.getString("nin_apellido"));
				ninos.add(nino);
				System.out.println(rs.getString("nin_cod"));
				
			}
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
		  closeClonnection();
		}
			
	  return ninos;
	}

}
