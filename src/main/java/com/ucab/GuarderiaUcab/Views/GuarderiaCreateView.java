package com.ucab.GuarderiaUcab.Views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class GuarderiaCreateView extends JFrame {

	public JPanel contentPane;
	public JTextField txtNombre;
	public JTextField txtRif;
	public JTextField txtTarifa;
	public JFrame FrInicio;
	public JButton btnRegistrar;
	public JButton btnRegresar;
	public JTextField textField;
	public JTextField txtTelefono;
	
	
	public GuarderiaCreateView() {
		initializerView();
	}
	
	public void initializerView(){
		

		
		
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nueva Guarderia");
		FrInicio.setBounds(100, 100, 450, 300);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		contentPane = new JPanel();		
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(0, 9, 424, 252);
		contentPane.setLayout(null);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(297, 216, 117, 25);
		contentPane.add(btnRegistrar);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(22, 27, 70, 15);
		contentPane.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setText("nombre");
		txtNombre.setBounds(22, 44, 114, 19);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblRif = new JLabel("Rif");
		lblRif.setBounds(268, 152, 80, 15);
		contentPane.add(lblRif);
		
		txtRif = new JTextField();
		txtRif.setText("rif");
		txtRif.setBounds(22, 93, 117, 19);
		contentPane.add(txtRif);
		txtRif.setColumns(10);
		
		JLabel lblTarifa = new JLabel("Tarifa");
		lblTarifa.setBounds(169, 74, 70, 15);
		contentPane.add(lblTarifa);
		
		txtTarifa = new JTextField();
		txtTarifa.setText("t ");
		txtTarifa.setBounds(169, 93, 117, 19);
		contentPane.add(txtTarifa);
		txtTarifa.setColumns(10);
		
		JLabel lblDireccion = new JLabel("Direccion");
		lblDireccion.setBounds(169,27,80, 15);
		lblRif.setBounds(22, 68, 70, 15);
		contentPane.add(lblDireccion);
		
		textField = new JTextField();
		textField.setBounds(169, 44, 117, 19);
		contentPane.add(textField);
		textField.setColumns(10);
		
		btnRegresar = new JButton("Regresar");
		btnRegresar.setBounds(10, 217, 89, 23);
		contentPane.add(btnRegresar);
		
		FrInicio.getContentPane().add(contentPane);
		
		JLabel lblTelefono = new JLabel("Telefono");
		lblTelefono.setBounds(22, 129, 46, 14);
		contentPane.add(lblTelefono);
		
		txtTelefono = new JTextField();
		txtTelefono.setText("telefono");
		txtTelefono.setBounds(22, 148, 114, 20);
		contentPane.add(txtTelefono);
		txtTelefono.setColumns(10);
		FrInicio.getContentPane().add(contentPane);

		FrInicio.setResizable(true);
	}
}
