package com.ucab.GuarderiaUcab.Views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class AutorizadoCreateView extends JFrame {

	public JPanel contentPane;
	
	public JFrame FrInicio;

	public JTextField txtCi;
	public JTextField txtNombre;
	public JTextField txtApellido;
	public JTextField txtCasa;
	public JTextField txtOficina;
	public JTextField txtCell;
	public JButton btnRegresar ;
	public JButton btnRegistrar;
	
	public AutorizadoCreateView() {
		InitializerView();
	}
	public void InitializerView() {
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nuevo autorizado");
		FrInicio.setBounds(100, 100, 450, 300);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		

		contentPane = new JPanel();
		contentPane.setBounds(0, 0, 424, 250);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		contentPane.setLayout(null);
		
		btnRegresar = new JButton("Regresar");
		btnRegresar.setBounds(10, 227, 89, 23);
		contentPane.add(btnRegresar);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(335, 227, 89, 23);
		contentPane.add(btnRegistrar);
		
		JLabel lblCedula = new JLabel("Cedula");
		lblCedula.setBounds(10, 11, 68, 23);
		contentPane.add(lblCedula);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 76, 68, 23);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(10, 150, 57, 20);
		contentPane.add(lblApellido);
		
		txtCi = new JTextField();
		txtCi.setText("ci");
		txtCi.setBounds(10, 45, 120, 20);
		contentPane.add(txtCi);
		txtCi.setColumns(10);
		
		txtNombre = new JTextField();
		txtNombre.setText("nombre");
		txtNombre.setBounds(10, 110, 120, 23);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setText("apellido");
		txtApellido.setBounds(10, 181, 120, 23);
		contentPane.add(txtApellido);
		txtApellido.setColumns(10);
		
		JLabel lblTelefonoCasa = new JLabel("Telefono Casa");
		lblTelefonoCasa.setBounds(222, 20, 89, 20);
		contentPane.add(lblTelefonoCasa);
		
		JLabel lblTelefonoOficina = new JLabel("Telefono Oficina");
		lblTelefonoOficina.setBounds(222, 80, 89, 20);
		contentPane.add(lblTelefonoOficina);
		
		JLabel lblTelefonoCelular = new JLabel("Telefono Celular");
		lblTelefonoCelular.setBounds(222, 153, 94, 23);
		contentPane.add(lblTelefonoCelular);
		
		txtCasa = new JTextField();
		txtCasa.setText("casa");
		txtCasa.setBounds(222, 45, 120, 20);
		contentPane.add(txtCasa);
		txtCasa.setColumns(10);
		
		txtOficina = new JTextField();
		txtOficina.setText("oficina");
		txtOficina.setBounds(222, 111, 120, 22);
		contentPane.add(txtOficina);
		txtOficina.setColumns(10);
		
		txtCell = new JTextField();
		txtCell.setText("cell");
		txtCell.setBounds(222, 182, 120, 23);
		contentPane.add(txtCell);
		txtCell.setColumns(10);
		
		FrInicio.getContentPane().add(contentPane);

		FrInicio.setResizable(true);
	}

}
