package com.ucab.GuarderiaUcab.Views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class JuegoCreateView extends JFrame {

	public JPanel contentPane;
	public JTextField txtNombre;
	public JButton btnRegistrar;
	public JButton btnRegresar;
	public JFrame FrInicio;


	public JuegoCreateView() {
		InitializerView();
	}
	public void InitializerView() {
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nueva juego");
		FrInicio.setBounds(100, 100, 450, 300);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		contentPane = new JPanel();		
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(0, 9, 424, 252);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(215, 37, 109, 33);
		contentPane.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(138, 82, 213, 39);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(191, 157, 89, 23);
		contentPane.add(btnRegistrar);
		
		btnRegresar = new JButton("Regresar");
		btnRegresar.setBounds(191, 205, 89, 23);
		contentPane.add(btnRegresar);
		
		FrInicio.getContentPane().add(contentPane);
		FrInicio.setResizable(true);
	}
}
