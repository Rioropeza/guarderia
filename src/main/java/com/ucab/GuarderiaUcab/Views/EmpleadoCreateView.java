package com.ucab.GuarderiaUcab.Views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class EmpleadoCreateView extends JFrame {

	public JFrame FrInicio;
	public JPanel contentPane;
	public JTextField txtCi;
	public JTextField txtNombre;
	public JTextField txtApellido;
	public JTextField txtTelefono;
	public JLabel lblTelefono;
	public JLabel lblEstudio;
	public JTextField txtEstudio;
	public JLabel lblExperiencia;
	public JTextField txtExperiencia;
	public JLabel lblSueldo;
	public JTextField textField;
	public JButton btnRegresar;
	public JButton btnRegistrar;

	 	
	public EmpleadoCreateView() {
		InitializerView();
	}
	
	public void InitializerView() {
		
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nuevo Empleado");
		FrInicio.setBounds(100, 100, 450, 333);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		
		contentPane = new JPanel();
		contentPane.setLocation(0, 0);
		contentPane.setSize(434, 300);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		
		btnRegresar = new JButton("Regresar");
		btnRegresar.setBounds(27, 261, 89, 23);
		contentPane.add(btnRegresar);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(344, 261, 89, 23);
		contentPane.add(btnRegistrar);
		
		JLabel lblCedula = new JLabel("Cedula");
		lblCedula.setBounds(27, 25, 46, 14);
		contentPane.add(lblCedula);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(27, 89, 46, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(27, 143, 46, 14);
		contentPane.add(lblApellido);
		
		txtCi = new JTextField();
		txtCi.setText("ci");
		txtCi.setBounds(27, 50, 107, 20);
		contentPane.add(txtCi);
		txtCi.setColumns(10);
		
		txtNombre = new JTextField();
		txtNombre.setText("nombre");
		txtNombre.setBounds(27, 114, 107, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setText("apellido");
		txtApellido.setBounds(27, 168, 107, 20);
		contentPane.add(txtApellido);
		txtApellido.setColumns(10);
		
		txtTelefono = new JTextField();
		txtTelefono.setText("telefono");
		txtTelefono.setBounds(188, 50, 107, 20);
		contentPane.add(txtTelefono);
		txtTelefono.setColumns(10);
		
		lblTelefono = new JLabel("Telefono");
		lblTelefono.setBounds(188, 25, 46, 14);
		contentPane.add(lblTelefono);
		
		lblEstudio = new JLabel("Estudio");
		lblEstudio.setBounds(188, 89, 66, 14);
		contentPane.add(lblEstudio);
		
		txtEstudio = new JTextField();
		txtEstudio.setText("estudio");
		txtEstudio.setBounds(188, 114, 107, 20);
		contentPane.add(txtEstudio);
		txtEstudio.setColumns(10);
		
		lblExperiencia = new JLabel("Experiencia");
		lblExperiencia.setBounds(188, 143, 66, 14);
		contentPane.add(lblExperiencia);
		
		txtExperiencia = new JTextField();
		txtExperiencia.setText("experiencia");
		txtExperiencia.setBounds(188, 168, 107, 20);
		contentPane.add(txtExperiencia);
		txtExperiencia.setColumns(10);
		
		lblSueldo = new JLabel("Sueldo");
		lblSueldo.setBounds(27, 214, 46, 14);
		contentPane.add(lblSueldo);
		
		textField = new JTextField();
		textField.setBounds(90, 211, 107, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		FrInicio.getContentPane().add(contentPane);

		FrInicio.setResizable(true);
	}
}
