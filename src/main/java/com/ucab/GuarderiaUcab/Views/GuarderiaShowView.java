package com.ucab.GuarderiaUcab.Views;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.JTable;


public class GuarderiaShowView extends JFrame {

	public JPanel contentPane;
	public JTextField text_id;
	public JTextField text_pass;
	public JButton btn_login;
	public JFrame  FrInicio;
	public JTable table;
    public JScrollPane jScrollPane;
    public JButton btn_eliminar;
    public JButton btn_agregar;
    public JButton btn_modificar;
    public JButton btn_ver;
    public DefaultTableModel tableModel;
    public Object[] col;
	
	public GuarderiaShowView() {
	
		initialization();
		ver_tabla();
	}
	
	private void initialization() {
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nueva Guarderia");
		FrInicio.setBounds(0, 0, 500, 500);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(0, 0, 500, 500);
		contentPane.setLayout(null);
		

        
	    jScrollPane = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
		table.setModel(new javax.swing.table.DefaultTableModel(
	            new Object [][] {
	                {null, null, null, null, null}
	            },
	            new String [] {
	                "Title 1", "Title 2", "Title 3", "Title 4", "Title 5"
	            }
	        ));
		
	    table.setBounds(28, 51, 382, 231);
	    
	    jScrollPane = new JScrollPane(table);
	    jScrollPane.setBounds(28, 51, 382, 231);
		contentPane.add(jScrollPane,BorderLayout.CENTER);
		
		FrInicio.getContentPane().add(contentPane);
		
		btn_agregar = new JButton("Agregar");
		btn_agregar.setBounds(293, 27, 117, 25);
		contentPane.add(btn_agregar);
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.setResizable(false);
	}
	
	   public void ver_tabla(){
	        
	        table.setDefaultRenderer(Object.class, new Render());
	        
	        btn_modificar = new JButton("Modificar");
	        btn_modificar.setName("m");
	        btn_ver = new JButton("Ver");
	        btn_ver.setName("v");
	        btn_eliminar = new JButton("Eliminar");
	        btn_eliminar.setName("e");
	        col = new Object[]{"Rif","Nombre","Telefono","Acciones","",""};
	        tableModel = new DefaultTableModel(new Object [][] {},col)
	        {
	            public boolean isCellEditable(int row, int column){
	                return false;
	            }
	        };
	        table.setModel(tableModel);
	        table.setPreferredScrollableViewportSize(table.getPreferredSize());
	  

	    }

}
