package com.ucab.GuarderiaUcab.Views;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;


public class DashboardView extends JFrame {

	private JPanel contentPane;
	public JFrame FrInicio;
	public JButton btnGuarderia;
	public JButton btnEmpleado;
	public JButton btnRepresentante;
	public JButton btnNino;
	public JButton btnAutorizado;
	public JButton btnJuego;
	public JButton btnActividad;
	public JButton btnEnfermedad;
	public JButton btnMedicina;
	public JButton btnAlergia;
	public JButton btnSintoma;
	
	public DashboardView() {
		initializerView();

	}
	public void initializerView(){
		FrInicio = new JFrame();
		FrInicio.setTitle("guarderias");
		FrInicio.setBounds(100, 100, 600, 406);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		//setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 642, 412);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(101, 12, 399, 364);
		contentPane.setLayout(null);
		
		btnGuarderia = new JButton("guarderia");
		btnGuarderia.setBounds(191, 185, 179, 25);
		contentPane.add(btnGuarderia);
		
		btnRepresentante = new JButton("representante");
		btnRepresentante.setBounds(12, 143, 167, 25);
		contentPane.add(btnRepresentante);
		
		btnEmpleado = new JButton("empleado");
		btnEmpleado.setBounds(191, 30, 179, 25);
		contentPane.add(btnEmpleado);
		
		btnNino = new JButton("nino");
		btnNino.setBounds(12, 103, 167, 25);
		contentPane.add(btnNino);
		
		btnAutorizado = new JButton("autorizado");
		btnAutorizado.setBounds(12, 30, 167, 25);
		contentPane.add(btnAutorizado);
		
		btnJuego = new JButton("juegos");
		btnJuego.setBounds(12, 185, 167, 25);
		contentPane.add(btnJuego);
		
		btnActividad = new JButton("actividad");
		btnActividad.setBounds(12, 67, 167, 25);
		contentPane.add(btnActividad);
		
		btnEnfermedad = new JButton("enfermedad");
		btnEnfermedad.setBounds(191, 143, 179, 25);
		contentPane.add(btnEnfermedad);
				
		btnMedicina = new JButton("medicina");
		btnMedicina.setBounds(122, 232, 140, 25);
		contentPane.add(btnMedicina);

		FrInicio.getContentPane().add(contentPane);
		
		btnAlergia = new JButton("alergia");
		btnAlergia.setBounds(191, 67, 179, 25);
		contentPane.add(btnAlergia);
		
		btnSintoma = new JButton("sintoma");
		btnSintoma.setBounds(191, 103, 179, 25);
		contentPane.add(btnSintoma);
	}
}
