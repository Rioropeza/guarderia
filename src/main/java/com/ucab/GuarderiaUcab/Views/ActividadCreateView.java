package com.ucab.GuarderiaUcab.Views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JSpinner;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JCheckBox;

public class ActividadCreateView extends JFrame {

	public JPanel contentPane;
	public JTextField txtNombre;
	public JTextField textField;
	public JCheckBox chckbxTransporte;
	public JButton btnRegresar;
	public JButton btnRegistrar;
	public JComboBox comboBox;
	public JFrame FrInicio;

		/**
	 * Create the frame.
	 */
	public ActividadCreateView() {
		InitializerView();
	}
	public void InitializerView() {
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nueva Actividad");
		FrInicio.setBounds(200, 233, 436, 262);	
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setBounds(100, 233, 436, 262);
		contentPane.setLayout(null);
		
		btnRegresar = new JButton("Regresar");
		btnRegresar.setBounds(10, 227, 89, 23);
		contentPane.add(btnRegresar);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(335, 227, 89, 23);
		contentPane.add(btnRegistrar);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 11, 46, 14);
		contentPane.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setText("nombre");
		txtNombre.setBounds(10, 36, 86, 20);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		JLabel lblEdadMinima = new JLabel("Edad Minima");
		lblEdadMinima.setBounds(227, 11, 89, 14);
		contentPane.add(lblEdadMinima);
		
		comboBox = new JComboBox();
		comboBox.setBounds(227, 36, 68, 20);
		contentPane.add(comboBox);
		comboBox.addItem(1);
		comboBox.addItem(2);
		comboBox.addItem(3);
		comboBox.addItem(4);
		comboBox.addItem(5);
		comboBox.addItem(6);
		

		
		JLabel lblDescripcion = new JLabel("Descripcion");
		lblDescripcion.setBounds(10, 78, 89, 23);
		contentPane.add(lblDescripcion);
		
		textField = new JTextField();
		textField.setBounds(78, 79, 222, 63);
		contentPane.add(textField);
		textField.setColumns(10);
		
		chckbxTransporte = new JCheckBox("Transporte");
		chckbxTransporte.setBounds(10, 156, 97, 23);
		contentPane.add(chckbxTransporte);
		FrInicio.getContentPane().add(contentPane);
		FrInicio.setResizable(true);
	}
}
