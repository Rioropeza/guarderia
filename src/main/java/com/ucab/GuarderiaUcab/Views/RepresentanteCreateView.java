package com.ucab.GuarderiaUcab.Views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class RepresentanteCreateView extends JFrame {

	public JPanel contentPane;
	public JTextField txtCi;
	public JButton btnRegresar;
	public JButton btnRegistrar;
	public JTextField txtNom;
	public JTextField txtApe;
	public JTextField txtMn;
	public JTextField txtProfesion;
	public JTextField txtEmail;
	public JTextField txtCasa;
	public JTextField txtOficina;
	public JTextField txtCell;
	public JTextField txtDireccion;
	public JFrame FrInicio;

	
	public RepresentanteCreateView() {
		InitializerView();
	}
	public void InitializerView() {
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nuevo Representante");
		FrInicio.setBounds(100, 100, 509, 327);
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		FrInicio.getContentPane().setLayout(null);
		
		
		setBounds(100, 100, 491, 300);
		contentPane = new JPanel();
		contentPane.setLocation(0, 0);
		contentPane.setSize(483, 277);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		
		contentPane.setLayout(null);
		
		btnRegresar = new JButton("Regresar");
		btnRegresar.setBounds(10, 227, 89, 23);
		contentPane.add(btnRegresar);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(376, 227, 89, 23);
		contentPane.add(btnRegistrar);
		
		JLabel lblCedula = new JLabel("Cedula");
		lblCedula.setBounds(10, 11, 89, 23);
		contentPane.add(lblCedula);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 63, 89, 23);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(10, 120, 89, 23);
		contentPane.add(lblApellido);
		
		JLabel lblProfesion = new JLabel("Profesion");
		lblProfesion.setBounds(172, 63, 89, 23);
		contentPane.add(lblProfesion);
		
		JLabel lblEstadoCivil = new JLabel("Estado Civil");
		lblEstadoCivil.setBounds(172, 11, 97, 23);
		contentPane.add(lblEstadoCivil);
		
		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(172, 124, 89, 19);
		contentPane.add(lblEmail);
		
		JLabel lblTelefonoCasa = new JLabel("Telefono Casa");
		lblTelefonoCasa.setBounds(334, 13, 89, 19);
		contentPane.add(lblTelefonoCasa);
		
		JLabel lblTelefonoOficina = new JLabel("Telefono Oficina");
		lblTelefonoOficina.setBounds(334, 65, 89, 19);
		contentPane.add(lblTelefonoOficina);
		
		JLabel lblTelefonoCelular = new JLabel("Telefono Celular");
		lblTelefonoCelular.setBounds(334, 122, 89, 19);
		contentPane.add(lblTelefonoCelular);
		
		txtCi = new JTextField();
		txtCi.setText("ci");
		txtCi.setBounds(10, 32, 97, 23);
		contentPane.add(txtCi);
		txtCi.setColumns(10);
		
		txtNom = new JTextField();
		txtNom.setText("nom");
		txtNom.setBounds(10, 89, 97, 27);
		contentPane.add(txtNom);
		txtNom.setColumns(10);
		
		txtApe = new JTextField();
		txtApe.setText("ape");
		txtApe.setBounds(10, 154, 97, 27);
		contentPane.add(txtApe);
		txtApe.setColumns(10);
		
		txtMn = new JTextField();
		txtMn.setText("m/n");
		txtMn.setBounds(172, 32, 112, 23);
		contentPane.add(txtMn);
		txtMn.setColumns(10);
		
		txtProfesion = new JTextField();
		txtProfesion.setText("profesion");
		txtProfesion.setBounds(172, 93, 112, 23);
		contentPane.add(txtProfesion);
		txtProfesion.setColumns(10);
		
		txtEmail = new JTextField();
		txtEmail.setText("email");
		txtEmail.setBounds(172, 154, 112, 27);
		contentPane.add(txtEmail);
		txtEmail.setColumns(10);
		
		txtCasa = new JTextField();
		txtCasa.setText("casa");
		txtCasa.setBounds(334, 32, 112, 23);
		contentPane.add(txtCasa);
		txtCasa.setColumns(10);
		
		txtOficina = new JTextField();
		txtOficina.setText("oficina");
		txtOficina.setBounds(334, 91, 112, 25);
		contentPane.add(txtOficina);
		txtOficina.setColumns(10);
		
		txtCell = new JTextField();
		txtCell.setText("cell");
		txtCell.setBounds(334, 157, 112, 24);
		contentPane.add(txtCell);
		txtCell.setColumns(10);
		
		txtDireccion = new JTextField();
		txtDireccion.setBounds(172, 217, 112, 20);
		contentPane.add(txtDireccion);
		txtDireccion.setColumns(10);
		
		JLabel lblDireccion = new JLabel("Direccion");
		lblDireccion.setBounds(172, 192, 46, 14);
		contentPane.add(lblDireccion);
		
		FrInicio.getContentPane().add(contentPane);

		FrInicio.setResizable(true);
	}

}
