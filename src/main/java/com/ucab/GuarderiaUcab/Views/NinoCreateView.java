package com.ucab.GuarderiaUcab.Views;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import com.toedter.calendar.JDateChooser;
import javax.swing.JTextField;


public class NinoCreateView extends JFrame {

	public JPanel contentPane;
	public JButton btnRegresar;
	public JButton btnRegistrar;
	public JDateChooser calendar;
	public JTextField txtNombre;
	public JTextField txtApellido;
	public JTextField txtMf;
	public JLabel lblSexo;
	public JTextField txtLetra;
	private JLabel lblCodigo;
	public JTextField txtPediatra;
	public JFrame FrInicio;

 

	/**
	 * Create the frame.
	 */
	public NinoCreateView() {
		
		FrInicio = new JFrame();
		FrInicio.setTitle("Nuevo nino");
		FrInicio.setBounds(200, 233, 436, 262);	
		FrInicio.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(null);
		
		 btnRegresar = new JButton("Regresar");
		btnRegresar.setBounds(10, 227, 89, 23);
		contentPane.add(btnRegresar);
		
		 btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(335, 227, 89, 23);
		contentPane.add(btnRegistrar);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(10, 11, 46, 14);
		contentPane.add(lblNombre);
		
		JLabel lblApellido = new JLabel("Apellido");
		lblApellido.setBounds(10, 67, 46, 14);
		contentPane.add(lblApellido);
		
		JLabel lblFechaNacimiento = new JLabel("Fecha Nacimiento");
		lblFechaNacimiento.setBounds(10, 126, 89, 14);
		contentPane.add(lblFechaNacimiento);
		
		calendar = new JDateChooser();
		calendar.setBounds(10, 148, 121, 23);
		contentPane.add(calendar);
		calendar.setDateFormatString("dd/MM/yyyy");
		
		txtNombre = new JTextField();
		txtNombre.setText("nombre");
		txtNombre.setBounds(10, 36, 121, 23);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		txtApellido = new JTextField();
		txtApellido.setText("apellido");
		txtApellido.setBounds(10, 92, 121, 23);
		contentPane.add(txtApellido);
		txtApellido.setColumns(10);
		
		txtMf = new JTextField();
		txtMf.setText("m/f");
		txtMf.setBounds(193, 37, 121, 23);
		contentPane.add(txtMf);
		txtMf.setColumns(10);
		
		lblSexo = new JLabel("Sexo");
		lblSexo.setBounds(193, 11, 46, 14);
		contentPane.add(lblSexo);
		
		txtLetra = new JTextField();
		txtLetra.setText("letra");
		txtLetra.setBounds(193, 93, 121, 22);
		contentPane.add(txtLetra);
		txtLetra.setColumns(10);
		
		lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(193, 67, 46, 14);
		contentPane.add(lblCodigo);
		
		FrInicio.getContentPane().add(contentPane);
		
		/*txtPediatra = new JTextField();
		txtPediatra.setText("pediatra");
		txtPediatra.setColumns(10);
		txtPediatra.setBounds(193, 148, 121, 22);
		contentPane.add(txtPediatra);*/
		
		JLabel label = new JLabel("pediatra");
		label.setBounds(193, 126, 46, 14);
		contentPane.add(label);
		FrInicio.setResizable(true);
	}
}
