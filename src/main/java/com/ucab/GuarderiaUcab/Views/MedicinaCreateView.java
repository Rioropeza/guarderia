package com.ucab.GuarderiaUcab.Views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

public class MedicinaCreateView extends JFrame {

	private JPanel contentPane;
	public JButton btnRegistrar;
	public JButton btnRegresar;
	private JTextField txtNombre;


	public MedicinaCreateView() {
		InitializerView();
	}
	public void InitializerView() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(215, 37, 109, 33);
		contentPane.add(lblNombre);
		
		txtNombre = new JTextField();
		txtNombre.setBounds(138, 82, 213, 39);
		contentPane.add(txtNombre);
		txtNombre.setColumns(10);
		
		btnRegistrar = new JButton("Registrar");
		btnRegistrar.setBounds(191, 157, 89, 23);
		contentPane.add(btnRegistrar);
		
		btnRegresar = new JButton("Regresar");
		btnRegresar.setBounds(191, 205, 89, 23);
		contentPane.add(btnRegresar);
	}



}
