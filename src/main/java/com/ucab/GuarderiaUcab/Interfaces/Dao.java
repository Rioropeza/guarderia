package com.ucab.GuarderiaUcab.Interfaces;

public interface Dao<T> {

	void create(T dao);

	void read(T dao);

	void update(T dao);

	void delete(T dao);
	
	void report(T dao);

}
